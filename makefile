PYTHON?=python3
APP_DIR?=inv
DATA_DIR?=data
STATIC_DIR?=$(APP_DIR)/static

DB_NAME?=base.sqlite
DEBUG_LOG?=true

run:
	DATABASE="$(DATA_DIR)/$(DB_NAME)" \
	DEBUG_LOG=$(DEBUG_LOG) \
	$(PYTHON) run.py

BULMA_VER=0.0.16
BULMA_URL=https://github.com/jgthms/bulma/archive/$(BULMA_VER).zip
FONTAWESOME_VER=4.5.0
FONTAWESOME_URL=https://github.com/FortAwesome/Font-Awesome/archive/v$(FONTAWESOME_VER).zip

.deploy_bulma:
	mkdir -p $(DATA_DIR)
	mkdir -p $(STATIC_DIR)/css
	curl -L -o $(DATA_DIR)/bulma.zip $(BULMA_URL)
	unzip -j $(DATA_DIR)/bulma.zip "bulma-$(BULMA_VER)/css/bulma.min*" -d $(STATIC_DIR)/css
	rm -v $(DATA_DIR)/bulma.zip
.deploy_fontawesome:
	mkdir -p $(DATA_DIR)
	mkdir -p $(STATIC_DIR)/css $(STATIC_DIR)/fonts
	curl -L -o $(DATA_DIR)/fontawesome.zip $(FONTAWESOME_URL)
	unzip -j $(DATA_DIR)/fontawesome.zip "Font-Awesome-$(FONTAWESOME_VER)/css/font-awesome.min*" -d $(STATIC_DIR)/css
	unzip -j $(DATA_DIR)/fontawesome.zip "Font-Awesome-$(FONTAWESOME_VER)/fonts/fontawesome-webfont.*" -d $(STATIC_DIR)/fonts
	rm -v $(DATA_DIR)/fontawesome.zip


deploy: .deploy_bulma .deploy_fontawesome

clean:
	rm -rvf $(DATA_DIR)/$(DB_NAME)
