from inv.launch import launch_app, launch_barcode, launch_db, launch_env

app = launch_app()
db = launch_db(app)
bc = launch_barcode()

launch_env(app, db)
