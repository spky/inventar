from sqlalchemy.ext.declarative import declared_attr

from inv import db, bc
from inv.forms import CategoryForm, ItemForm, LabelForm, UserForm


class BaseMixin:
    id = db.Column(db.Integer, primary_key=True)
    ident = db.Column(db.String)

    @declared_attr
    def __name__(cls):
        return cls.__name__

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def __repr__(cls):
        return '({} #{} {})'.format(cls.__name__, cls.id, cls.ident)


class CommonMixin:
    name = db.Column(db.String)
    description = db.Column(db.String)


class Category(BaseMixin, CommonMixin, db.Model):
    _form = CategoryForm


class Item(BaseMixin, CommonMixin, db.Model):
    _form = ItemForm

    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category', backref=db.backref(
        'items', lazy='dynamic', cascade='all'
    ), post_update=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref(
        'items', lazy='dynamic', cascade='all'
    ), post_update=True)


class Label(BaseMixin, db.Model):
    _form = LabelForm

    name = db.Column(db.String)

    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    item = db.relationship('Item', backref=db.backref(
        'labels', lazy='dynamic', cascade='all,delete'
    ), post_update=True)

    @property
    def barcode(self):
        return bc(self.name)


class User(BaseMixin, CommonMixin, db.Model):
    _form = UserForm


BINDS = dict([(b.__tablename__, b) for b in [Category, Item, Label, User]])
NAMES = dict([(n, b.__name__) for n, b in BINDS.items()])
