from os import getenv

ENV_TRUE = ['1', 'true', 'y']


def read_env(name, fallback, as_bool=False):
    val = str(getenv(name.upper(), fallback))
    if as_bool:
        return True if val.lower() in [e.lower() for e in ENV_TRUE] else False
    return val
