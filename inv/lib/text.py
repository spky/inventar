from random import choice
from string import (
    ascii_lowercase, ascii_uppercase, digits, punctuation, whitespace
)


def contains_whitespace(st):
    return any([s in whitespace for s in st])


def contains_only_lower(st):
    return st.lower() == st


def contains_only_ascii(st, numbers=True):
    seed = ascii_lowercase
    seed += digits if numbers else ''
    return all([s in seed for s in st.lower()])


def ident_ify(st):
    seed = ascii_lowercase + digits
    return ''.join([s for s in st.strip().lower() if s in seed])


def generate_str(length=64, lower=True, upper=False, numbers=True, dots=False):
    seed = ''
    seed += ascii_lowercase if lower else ''
    seed += ascii_uppercase if upper else ''
    seed += digits if numbers else ''
    seed += punctuation if dots else ''
    return ''.join([choice(seed) for _ in range(length+1)])
