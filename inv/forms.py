from flask.ext.wtf import Form
from wtforms import SelectField, StringField, SubmitField, TextAreaField
from wtforms.validators import (
    DataRequired, InputRequired, Length, ValidationError
)

from inv.lib.text import contains_only_ascii, contains_whitespace


def _common_fields(name):
    return (
        StringField(
            'Name', description='{} Name'.format(name),
            validators=[DataRequired(), Length(min=3, max=64)]
        ),
        TextAreaField(
            'Description', description='{} Description'.format(name),
            validators=[Length(min=3, max=1024)]
        )
    )


def _ref_field(name, rname, required=True):
    return SelectField(
        rname, description='{} {}'.format(name, rname),
        coerce=int, validators=([InputRequired()] if required else [])
    )


class CategoryForm(Form):
    name, description = _common_fields('Category')
    save = SubmitField('Save')


class ItemForm(Form):
    name, description = _common_fields('Item')
    category_id = _ref_field('Item', 'Category')
    user_id = _ref_field('Item', 'User')

    save = SubmitField('Save')


class LabelForm(Form):
    name = StringField(
        'Label', description='Item Label',
        validators=[DataRequired(), Length(min=3, max=64)]
    )
    item_id = _ref_field('Label', 'Item', required=False)
    save = SubmitField('Save')

    def validate_ident(form, field):
        if contains_whitespace(field.data):
            raise ValidationError('No whitespace allowed')
        if not contains_only_ascii(field.data):
            raise ValidationError('Please avoid non-ascii characters')


class SearchForm(Form):
    query = StringField('Search', description='Search')
    search = SubmitField('Search')


class UserForm(Form):
    name, description = _common_fields('User')
    save = SubmitField('Save')
