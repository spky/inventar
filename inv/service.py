from sqlalchemy.exc import OperationalError

from inv import app, db
from inv.models import Category, Item, Label, User


def check_db():
    try:
        app.logger.info(' | '.join(sorted([
            '{}: {}'.format(M.__name__, M.query.count()) for M in [
                Category, Item, Label, User
            ]
        ])))
    except OperationalError as e:
        app.logger.warning(str(e))
        db.create_all()
