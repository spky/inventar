from flask import flash, render_template, request

from inv import app
from inv.forms import SearchForm
from inv.models import BINDS


def search_form():
    return SearchForm(formdata=request.form)


app.jinja_env.globals.update(search_form=search_form)


@app.route('/search', methods=['GET', 'POST'])
def search():
    form = search_form()
    results = dict()

    if request.method == 'POST' and form.validate_on_submit():
        query = form.query.data.strip()
        for name, Elem in BINDS.items():
            res = Elem.query.filter(
                Elem.ident.ilike('%{}%'.format(query))
            ).all()
            if res:
                results[name] = res
        flash('Nothing found', 'danger') if not results else ''

    return render_template(
        'search.html',
        form=form, results=results
    )
