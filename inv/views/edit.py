from flask import flash, redirect, render_template, request, url_for

from inv import app, db
from inv.lib.text import ident_ify
from inv.models import BINDS
from inv.views.helpers import backpopulate_form, redirect_elemident


@app.route('/edit/<string:elem>/<string:ident>', methods=['GET', 'POST'])
@app.route('/edit/<string:elem>', methods=['GET', 'POST'])
def edit(elem, ident=None):
    Elem, rdr = redirect_elemident('edit', elem, ident)
    if rdr:
        return rdr

    elem = None
    if ident:
        elem = Elem.query.filter_by(ident=ident).first()
        if not elem:
            elem = Elem(ident=ident, name=ident)
    elem = Elem() if not elem else elem
    form = Elem._form(formdata=request.form, obj=elem)

    backpopulate_form(form, 'item_id', BINDS['item'], prepend_void=(
        not not (Elem.__tablename__ == 'label')
    ))
    backpopulate_form(form, 'category_id', BINDS['category'])
    backpopulate_form(form, 'user_id', BINDS['user'])

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(elem)
        if getattr(form, 'name', None):
            elem.ident = ident_ify(form.name.data)
        elem.ident = ident_ify(elem.ident)

        db.session.add(elem)
        db.session.commit()
        flash('Saved: {} {}'.format(
            Elem.__name__, getattr(elem, 'name', elem.ident)
        ), 'success')
        return redirect(url_for(
            'index', elem=Elem.__tablename__, ident=elem.ident
        ))

    return render_template('edit.html', form=form)
