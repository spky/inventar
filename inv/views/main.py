from flask import abort, render_template

from inv import app
from inv.views.helpers import redirect_elemident


@app.route('/<string:elem>/<string:ident>')
@app.route('/<string:elem>')
@app.route('/')
def index(elem=None, ident=None):
    elem = elem if elem else app.config['ENTRY_PAGE']
    Elem, rdr = redirect_elemident('index', elem, ident)
    if rdr:
        return rdr

    if ident:
        elem = Elem.query.filter_by(ident=ident).first()
        return (
            render_template('element.html', content=elem)
            if elem else abort(404)
        )
    return render_template('element.html', content=Elem.query.all())
