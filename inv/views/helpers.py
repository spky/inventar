from flask import abort, redirect, url_for

from inv import app
from inv.lib.text import contains_only_lower
from inv.models import BINDS, NAMES

app.jinja_env.globals.update(NAMES=NAMES)


def backpopulate_form(form, name, Elem, prepend_void=False):
    field = getattr(form, name, None)
    if field:
        field.choices = (
            [(0, '__')] if prepend_void else []
        ) + [
            (e.id, e.name) for e in Elem.query.all()
        ]


def redirect_elemident(tgt, elem, ident=None):
    if not all([
        contains_only_lower(f) for f in ([elem, ident] if ident else [elem])
    ]):
        return (None, redirect(url_for(
            tgt,
            elem=elem.lower(),
            ident=ident.lower() if ident else None
        )))
    Elem = BINDS.get(elem)
    return (Elem, None) if Elem else abort(404)
