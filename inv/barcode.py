from operator import eq, ge


class Code128:
    def __init__(self, caption=False, height=100, qzone=True, thick=3):
        self.caption = caption
        self.height = height
        self.qzone = qzone
        self.thick = thick

        self.a, self.b, self.c, self._wdt,  = dict(), dict(), dict(), dict()
        for n, v in enumerate(self.CODE):
            self.a[v['a']], self.b[v['b']], self.c[v['c']] = n, n, n
            self._wdt[n] = v['w']

    def __ctrl(self, txt, s):
        for n in [n for n in [
            txt.find(c, s) for c in self.CTRL
        ] if n >= 0]:
            return all([t not in self.STRL for t in txt[s:n]])

    def __head(self, txt):
        code, first = self.b, self.b['StartB']
        if any([(
            txt[:l].isdigit() and op(len(txt), l)
        ) for l, op in [(2, eq), (4, ge)]]):
            code, first = self.c, self.c['StartC']
        elif self.__ctrl(txt, 0):
            code, first = self.a, self.a['StartA']
        return code, first

    def __bars(self, txt):
        p, lng = 0, len(txt)
        code, first = self.__head(txt)

        yield first
        while p < lng:
            if all([
                txt[p] == '~', (lng-p > 1)
            ]):
                if txt[p+1] in [str(i) for i in range(1, 4+1)]:
                    yield code['FNC{}'.format(txt[p+1])]
                    p += 2
                else:
                    p += 1

            if ((code in [self.a, self.b]) and (ord(txt[p]) >= 128)):
                yield code['FNC4']
                txt = txt.replace(txt[p], chr(ord(txt[p]) - 128), 1)

            if ((code in [self.c]) and (txt[p:p+2].isdigit()) and (lng-p > 1)):
                yield int(txt[p:p+2])
                p += 2
            elif (
                code in [self.a, self.b] and any([(
                    txt[p:p+l].isdigit() and op(lng-p, l)
                ) for l, op in [(4, eq), (6, ge)]])
            ):
                yield code['CodeC']
                code = self.c
            elif ((code in [self.b, self.c]) and self.__ctrl(txt, p)):
                yield code['CodeA']
                code = self.a
            elif (code in [self.c] or (
                code in [self.a] and ord(txt[p]) >= 96
            )):
                yield code['CodeB']
                code = self.b
            else:
                yield code[txt[p]]
                p += 1

        yield txt

    def _code(self, txt):
        *bars, txt = self.__bars(txt)
        return bars + [
            sum([(max(n, 1) * b) for n, b in enumerate(bars)]) % 103,
            0x6A
        ], txt

    def __widths(self, code):
        return [
            (int(w) * self.thick) for c in code for w in str(self._wdt[c])
        ]

    def _svg(self, txt, code):
        widths = self.__widths(code)
        width = sum(widths)
        height = self.height

        x = 10 * self.thick if self.qzone else 0
        width += (20 * self.thick) if self.qzone else 0
        height_e = height + (
            (height//40 + height//7) if self.caption else 0
        )

        yield '<?xml version="1.0" encoding="UTF-8"?>'
        yield (
            '<svg width="{w}px" height="{h}px" viewbox="0 0 {w} {h}" '
            'xmlns="http://www.w3.org/2000/svg"'
            '>'.format(w=width, h=height_e)
        )
        yield (
            '    <rect width="{w}" height="{h}" fill="white"/'
            '>'.format(w=width, h=height)
        )

        toggle = True
        for w in widths:
            if toggle:
                yield (
                    '    <rect x="{x}" width="{w}" height="{h}"/'
                    '>'.format(x=x, w=w, h=height)
                )
            toggle = (not toggle)
            x += w

        if self.caption:
            yield (
                '   <text x="{x}" y="{y}" text-anchor="middle" '
                'font-size="{s}px" font-family="monospace">{t}</text'
                '>'.format(x=(width//2), y=height_e, s=(height//7), t=txt)
            )

        yield '</svg>'

    def __call__(self, text):
        text = str(text).encode('latin-1', 'replace').decode('latin-1')
        code, text = self._code(text)
        return '\n'.join(self._svg(text, code))

    CTRL = [chr(i) for i in range(32)]
    STRL = [chr(i) for i in range(96, 128)]
    CODE = [
        dict(a=' ',       b=' ',      c='00',     w=212222),
        dict(a='!',       b='!',      c='01',     w=222122),
        dict(a='"',       b='"',      c='02',     w=222221),
        dict(a='#',       b='#',      c='03',     w=121223),
        dict(a='$',       b='$',      c='04',     w=121322),
        dict(a='%',       b='%',      c='05',     w=131222),
        dict(a='&',       b='&',      c='06',     w=122213),
        dict(a='\'',      b='\'',     c='07',     w=122312),
        dict(a='(',       b='(',      c='08',     w=132212),
        dict(a=')',       b=')',      c='09',     w=221213),
        dict(a='*',       b='*',      c='10',     w=221312),
        dict(a='+',       b='+',      c='11',     w=231212),
        dict(a=',',       b=',',      c='12',     w=112232),
        dict(a='-',       b='-',      c='13',     w=122132),
        dict(a='.',       b='.',      c='14',     w=122231),
        dict(a='/',       b='/',      c='15',     w=113222),
        dict(a='0',       b='0',      c='16',     w=123122),
        dict(a='1',       b='1',      c='17',     w=123221),
        dict(a='2',       b='2',      c='18',     w=223211),
        dict(a='3',       b='3',      c='19',     w=221132),
        dict(a='4',       b='4',      c='20',     w=221231),
        dict(a='5',       b='5',      c='21',     w=213212),
        dict(a='6',       b='6',      c='22',     w=223112),
        dict(a='7',       b='7',      c='23',     w=312131),
        dict(a='8',       b='8',      c='24',     w=311222),
        dict(a='9',       b='9',      c='25',     w=321122),
        dict(a=':',       b=':',      c='26',     w=321221),
        dict(a=';',       b=';',      c='27',     w=312212),
        dict(a='<',       b='<',      c='28',     w=322112),
        dict(a='=',       b='=',      c='29',     w=322211),
        dict(a='>',       b='>',      c='30',     w=212123),
        dict(a='?',       b='?',      c='31',     w=212321),
        dict(a='@',       b='@',      c='32',     w=232121),
        dict(a='A',       b='A',      c='33',     w=111323),
        dict(a='B',       b='B',      c='34',     w=131123),
        dict(a='C',       b='C',      c='35',     w=131321),
        dict(a='D',       b='D',      c='36',     w=112313),
        dict(a='E',       b='E',      c='37',     w=132113),
        dict(a='F',       b='F',      c='38',     w=132311),
        dict(a='G',       b='G',      c='39',     w=211313),
        dict(a='H',       b='H',      c='40',     w=231113),
        dict(a='I',       b='I',      c='41',     w=231311),
        dict(a='J',       b='J',      c='42',     w=112133),
        dict(a='K',       b='K',      c='43',     w=112331),
        dict(a='L',       b='L',      c='44',     w=132131),
        dict(a='M',       b='M',      c='45',     w=113123),
        dict(a='N',       b='N',      c='46',     w=113321),
        dict(a='O',       b='O',      c='47',     w=133121),
        dict(a='P',       b='P',      c='48',     w=313121),
        dict(a='Q',       b='Q',      c='49',     w=211331),
        dict(a='R',       b='R',      c='50',     w=231131),
        dict(a='S',       b='S',      c='51',     w=213113),
        dict(a='T',       b='T',      c='52',     w=213311),
        dict(a='U',       b='U',      c='53',     w=213131),
        dict(a='V',       b='V',      c='54',     w=311123),
        dict(a='W',       b='W',      c='55',     w=311321),
        dict(a='X',       b='X',      c='56',     w=331121),
        dict(a='Y',       b='Y',      c='57',     w=312113),
        dict(a='Z',       b='Z',      c='58',     w=312311),
        dict(a='[',       b='[',      c='59',     w=332111),
        dict(a='\\',      b='\\',     c='60',     w=314111),
        dict(a=']',       b=']',      c='61',     w=221411),
        dict(a='^',       b='^',      c='62',     w=431111),
        dict(a='_',       b='_',      c='63',     w=111224),
        dict(a='\N{NUL}', b='`',      c='64',     w=111422),
        dict(a='\N{SOH}', b='a',      c='65',     w=121124),
        dict(a='\N{STX}', b='b',      c='66',     w=121421),
        dict(a='\N{ETX}', b='c',      c='67',     w=141122),
        dict(a='\N{EOT}', b='d',      c='68',     w=141221),
        dict(a='\N{ENQ}', b='e',      c='69',     w=112214),
        dict(a='\N{ACK}', b='f',      c='70',     w=112412),
        dict(a='\N{BEL}', b='g',      c='71',     w=122114),
        dict(a='\N{BS}',  b='h',      c='72',     w=122411),
        dict(a='\t',      b='i',      c='73',     w=142112),
        dict(a='\n',      b='j',      c='74',     w=142211),
        dict(a='\v',      b='k',      c='75',     w=241211),
        dict(a='\f',      b='l',      c='76',     w=221114),
        dict(a='\r',      b='m',      c='77',     w=413111),
        dict(a='\N{SO}',  b='n',      c='78',     w=241112),
        dict(a='\N{SI}',  b='o',      c='79',     w=134111),
        dict(a='\N{DLE}', b='p',      c='80',     w=111242),
        dict(a='\N{DC1}', b='q',      c='81',     w=121142),
        dict(a='\N{DC2}', b='r',      c='82',     w=121241),
        dict(a='\N{DC3}', b='s',      c='83',     w=114212),
        dict(a='\N{DC4}', b='t',      c='84',     w=124112),
        dict(a='\N{NAK}', b='u',      c='85',     w=124211),
        dict(a='\N{SYN}', b='v',      c='86',     w=411212),
        dict(a='\N{ETB}', b='w',      c='87',     w=421112),
        dict(a='\N{CAN}', b='x',      c='88',     w=421211),
        dict(a='\x19',    b='y',      c='89',     w=212141),
        dict(a='\N{SUB}', b='z',      c='90',     w=214121),
        dict(a='\N{ESC}', b='{',      c='91',     w=412121),
        dict(a='\x1C',    b='|',      c='92',     w=111143),
        dict(a='\x1D',    b='}',      c='93',     w=111341),
        dict(a='\x1E',    b='~',      c='94',     w=131141),
        dict(a='\x1F',    b='DEL',    c='95',     w=114113),
        dict(a='FNC3',    b='FNC3',   c='96',     w=114311),
        dict(a='FNC2',    b='FNC2',   c='97',     w=411113),
        dict(a='ShiftB',  b='ShiftA', c='98',     w=411311),
        dict(a='CodeC',   b='CodeC',  c='99',     w=113141),
        dict(a='CodeB',   b='FNC4',   c='CodeB',  w=114131),
        dict(a='FNC4',    b='CodeA',  c='CodeA',  w=311141),
        dict(a='FNC1',    b='FNC1',   c='FNC1',   w=411131),
        dict(a='StartA',  b='StartA', c='StartA', w=211412),
        dict(a='StartB',  b='StartB', c='StartB', w=211214),
        dict(a='StartC',  b='StartC', c='StartC', w=211232),
        dict(a='Stop',    b='Stop',   c='Stop',   w=2331112),
    ]
