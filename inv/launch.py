from logging import DEBUG, Formatter, StreamHandler
from os import path

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from inv.barcode import Code128
from inv.lib.system import read_env
from inv.lib.text import generate_str


def log(app):
    handler = StreamHandler(stream=None)
    formatter = Formatter('\
%(levelname)s [%(module)s.%(funcName)s:%(lineno)d] (%(asctime)s):\n\
    %(message)s'.strip())
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
    if any([
        app.debug,
        read_env('DEBUG_LOG', True, as_bool=True)
    ]):
        app.logger.setLevel(DEBUG)


def launch_app():
    name = __name__.split('.')[0]
    database = 'sqlite:///{}'.format(
        path.realpath(read_env('DATABASE', 'data/default.db'))
    )
    entry = read_env('ENTRY_PAGE', 'item')
    secret = read_env('SECRET_KEY', generate_str(
        length=256, lower=True, upper=True, numbers=True, dots=True
    ))

    app = Flask(name)
    app.config['APP_NAME'] = name
    app.config['ENTRY_PAGE'] = entry
    app.config['SECRET_KEY'] = secret
    app.config['SQLALCHEMY_DATABASE_URI'] = database
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    log(app)
    return app


def launch_db(app):
    return SQLAlchemy(app)


def launch_barcode():
    return Code128(caption=False, qzone=False, height=75)


def launch_env(app, db):
    from inv.views import edit, main, search
    from inv import service
    service.check_db()
